# <, >, <=, >=, ==, and != can be used to compare two variables
# 
# not returns opposite of bool value of a variable
# and returns true if both are true, false if one or both are false
# or returns true if one of the two are true 
# 
# branching program
# code executes -> test -> theres a code block if the test is true -> block of code if the test is false -> break test and return to program
# dont necessarily need a false block
# 
# python seperates code blocks using indentation and the colon
# 
# elif will allow for daisychained if statements