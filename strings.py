# 
# strings are simply letters that are enclosed in single or double quotes that can be used as variables
# 
# can combine strings by using print(first + second)
# no space will be in between the letters
# 
# 3 * string will return concatination of three strings
# 
# len() returns the amount of characters in a string
# 
# string[0] will return the first letter in a string
# 
# string[1:3] will return everything in between the 2nd and 4rd element including the 2nd element
# first number will be included, second number is not
# 
# string[:-1] will return everything from the first letter to the last not including the last
# string[::2] will return every other thing in the string, 2 is the stepping 
