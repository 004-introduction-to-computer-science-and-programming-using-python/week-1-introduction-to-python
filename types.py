# definitions are evaluated
# expressions are executed
# 
# data is represented by objects
# types define what a data point can do 
# objects can be scalar or nonscalar
# scalar means can't be subdivided
# 
# scalar objects
# ints
# floats
# bool
# NoneType
# 
# type() returns type of the argument, type(5) would return int
# 
# casting changes the type of equivalent data points
# float(5) would return 5.0
# 
# int division uses // and returns the quotient without a remainder
