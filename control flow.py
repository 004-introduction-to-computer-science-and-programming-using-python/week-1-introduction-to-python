#
# simple branches
# if ********:
#   code code code
# 
# if ********:
#   code code code
# else *****:
#   code code code
# 
# if ********:
#   code code code
# elif *****:
#   code code code
# else *****:
#   code code code
# 
# can only go through the flow once, not repeating
# 
# While Loops
# syntax
# while (conditional): 
#   code code code
# 
# will stop when the conditional is no longer true
# 
# range(x) will return integers starting at 0 and ending at x
# 
# For Loops
# 
# for x in index:
#   code code code
# 
# break can be used to stop a for loop early
# 
# for i in x:
#   code code code
#   break
#   code code code
# 
# will never execute the second code block 
#  
# range() uses the same (start, stop, stepping) formula as indexing strings except with commas
# 
# for vs while
# 
# for: set number of iterations, ends early using break, uses a counter
# while: unbound number of interations, ends early using break, can use a counter that is initialized outside of the loop and incremented inside of loop (dont need to but can) 
